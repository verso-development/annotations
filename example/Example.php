<?php
/*
 * Copyright (c) 2020, Juan (juan.valero@etu.univ-lyon1.fr), All rights reserved
 */

namespace Example;

use Example\Annotations\BasicAnnotation;
use Example\Annotations\ComplexAnnotation as Complex;

/**
 * An annotated class with a small description
 *
 * @package Example
 *
 * @BasicAnnotation(array={"zero": 0, "two": 2, "four": 4})
 * @Complex(string="A basic \"quoted\" string", int=4)
 */
class Example
{

}
