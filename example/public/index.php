<?php
/*
 * Copyright (c) 2020, Juan (juan.valero@etu.univ-lyon1.fr), All rights reserved
 */

use Annotations\AnnotationReader;
use Annotations\Exceptions\AnnotationException;
use Example\Annotations\BasicAnnotation;
use Example\Annotations\ComplexAnnotation;
use Example\Example;

require '../../vendor/autoload.php';
require '../functions.php';

try {
    $reader = new AnnotationReader();

    $annotations = $reader->getClassAnnotations(new ReflectionClass(Example::class));

    $complexAnnotation = $annotations[ComplexAnnotation::class];

    debug($complexAnnotation->int);
    debug($complexAnnotation->string);

    $basicAnnotation = $annotations[BasicAnnotation::class];

    debug($basicAnnotation->array);
} catch (ReflectionException $ignored) {
} catch (AnnotationException $e) {
    echo $e->getMessage();
}
