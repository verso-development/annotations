<?php
/*
 * Copyright (c) 2020, Juan (juan.valero@etu.univ-lyon1.fr), All rights reserved
 */

namespace Example\Annotations;

use Annotations\Annotations\Annotation;
use Annotations\Annotations\Regex;
use Annotations\Annotations\Target;

/**
 * Class ComplexAnnotation
 * @package Example
 *
 * @Target(value=["CLASS"])
 *
 * @Annotation
 */
class ComplexAnnotation
{
    /**
     * Facultative argument
     *
     * @var int
     */
    public int $int = 10;

    /**
     * Required argument
     *
     * @var string
     *
     * @Regex("/.{3,}/") - At least 3 characters
     */
    public string $string;
}
