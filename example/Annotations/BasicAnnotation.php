<?php
/*
 * Copyright (c) 2020, Juan (juan.valero@etu.univ-lyon1.fr), All rights reserved
 */

namespace Example\Annotations;

use Annotations\Annotations\Annotation;

/**
 * Class BasicAnnotation
 * @package Example
 *
 * @Annotation
 */
class BasicAnnotation
{
    /**
     * Facultative argument
     *
     * @var array|int[]
     */
    public array $array = [0, 2, 4];
}
