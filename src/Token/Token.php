<?php
/*
 * Copyright (c) 2020, Juan (juan.valero@etu.univ-lyon1.fr), All rights reserved
 */

namespace Annotations\Token;

class Token
{
    private TokenType $type;
    private string $value;

    /**
     * Token constructor.
     * @param TokenType $type
     * @param string $value
     */
    public function __construct(TokenType $type, string $value)
    {
        $this->type = $type;
        $this->value = $value;
    }

    /**
     * @return TokenType
     */
    public function getType(): TokenType
    {
        return $this->type;
    }

    /**
     * @return string
     */
    public function getValue(): string
    {
        return $this->value;
    }
}
