<?php
/*
 * Copyright (c) 2020, Juan (juan.valero@etu.univ-lyon1.fr), All rights reserved
 */

namespace Annotations\Token;

class TokenType
{
    private int $id;
    private string $name;
    private string $pattern;

    /**
     * TokenType constructor.
     * @param int $id
     * @param string $name
     * @param string $pattern
     */
    public function __construct(int $id, string $name, string $pattern)
    {
        $this->id = $id;
        $this->name = $name;
        $this->pattern = $pattern;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getPattern(): string
    {
        return $this->pattern;
    }
}
