<?php
/*
 * Copyright (c) 2020, Juan (juan.valero@etu.univ-lyon1.fr), All rights reserved
 */

namespace Annotations\Exceptions;

/**
 * Class ClassNotFoundException
 * Throw when the annotation class couldn't be found
 *
 * @package Annotation\Exceptions
 */
class ClassNotFoundException extends AnnotationException
{
    /**
     * ClassNotFoundException constructor.
     */
    public function __construct()
    {
        parent::__construct("The annotation couldn't be resolved");
    }
}
