<?php
/*
 * Copyright (c) 2020, Juan (juan.valero@etu.univ-lyon1.fr), All rights reserved
 */

namespace Annotations\Exceptions\Parsing;

/**
 * Class UnknownTokenException
 * Throws when the current token is unknown
 *
 * @package Annotations\Exceptions\Parsing
 */
class UnknownTokenException extends ParsingException
{
    /**
     * UnknownTokenException constructor.
     * @param string $lookahead
     * @param int $index
     * @param string $input
     */
    public function __construct(string $input, int $index, string $lookahead)
    {
        parent::__construct(
            sprintf(
                'Unknown token "%s" at index %d : %s <-- HERE',
                $lookahead,
                $index,
                $input
            )
        );
    }
}