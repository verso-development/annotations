<?php
/*
 * Copyright (c) 2020, Juan (juan.valero@etu.univ-lyon1.fr), All rights reserved
 */

namespace Annotations\Exceptions\Parsing;

/**
 * Class UnexpectedTokenException
 * Throws when the current token is not part of expected ones
 *
 * @package Annotation\Exceptions\Parsing
 */
class UnexpectedTokenException extends ParsingException
{
    /**
     * UnexpectedTokenException constructor.
     * @param string $input
     * @param int $index
     * @param string $lookahead
     * @param array $expectedTokens
     */
    public function __construct(string $input, int $index, string $lookahead, array $expectedTokens)
    {
        $tokenList = implode(' or ', $expectedTokens);
        $input = str_replace(' ', '&nbsp', $input); // Print the actual number of whitespaces

        parent::__construct(
            sprintf(
                'Expected token(s) %s, found %s at index %d : %s <-- HERE',
                $tokenList,
                $lookahead,
                $index - 1,
                $input
            )
        );
    }
}
