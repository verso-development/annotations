<?php
/*
 * Copyright (c) 2020, Juan (juan.valero@etu.univ-lyon1.fr), All rights reserved
 */

namespace Annotations\Exceptions\Parsing;

use Annotations\Exceptions\AnnotationException;

abstract class ParsingException extends AnnotationException
{

}