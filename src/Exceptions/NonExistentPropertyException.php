<?php
/*
 * Copyright (c) 2020, Juan (juan.valero@etu.univ-lyon1.fr), All rights reserved
 */

namespace Annotations\Exceptions;

use Annotations\Parser\Annotation\Argument;

/**
 * Class NonExistentPropertyException
 * Throws when an {@link Argument} is not present in the annotation arguments
 *
 * @package Annotation\Exceptions
 */
class NonExistentPropertyException extends AnnotationException
{
    /**
     * NonExistentPropertyException constructor.
     * @param string $annotation
     * @param string $property
     */
    public function __construct(string $annotation, string $property)
    {
        parent::__construct(sprintf('%s has no property with name "%s"', $annotation, $property));
    }
}
