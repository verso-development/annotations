<?php
/*
 * Copyright (c) 2020, Juan (juan.valero@etu.univ-lyon1.fr), All rights reserved
 */

namespace Annotations\Exceptions;

/**
 * Class MissingArgumentsException
 * Throws when a required argument is missing
 *
 * @package Annotation\Exceptions
 */
class MissingArgumentsException extends AnnotationException
{
    /**
     * MissingArgumentsException constructor.
     * @param string ...$arguments
     */
    public function __construct(string...$arguments)
    {
        $argumentsList = implode(', ', $arguments);
        parent::__construct(sprintf('Missing the following arguments : [%s]', $argumentsList));
    }
}
