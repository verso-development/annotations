<?php
/*
 * Copyright (c) 2020, Juan (juan.valero@etu.univ-lyon1.fr), All rights reserved
 */

namespace Annotations\Exceptions;

use Annotations\Annotations\RuledAnnotation;

/**
 * Class NotValidatedRuleException
 * Throws when a rule of a {@link RuledAnnotation} hasn't been validated
 *
 * @package Annotation\Exceptions
 */
class NotValidatedRuleException extends AnnotationException
{
    /**
     * NotValidatedRuleException constructor.
     * @param string $ruleErrorMessage
     */
    public function __construct(string $ruleErrorMessage)
    {
        parent::__construct(
            sprintf('AnnotationRule has not been validated. AnnotationRule message : %s', $ruleErrorMessage)
        );
    }
}
