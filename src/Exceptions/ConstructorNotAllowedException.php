<?php
/*
 * Copyright (c) 2020, Juan (juan.valero@etu.univ-lyon1.fr), All rights reserved
 */

namespace Annotations\Exceptions;

/**
 * Class ConstructorNotAllowedException
 * Throws when the annotation class has one argument or more and has a constructor
 *
 * @package Annotation\Exceptions
 */
class ConstructorNotAllowedException extends AnnotationException
{
    /**
     * ConstructorNotAllowedException constructor.
     */
    public function __construct()
    {
        parent::__construct('Annotations classes cannot have a constructor when they have 1 or more properties');
    }
}
