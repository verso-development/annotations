<?php
/*
 * Copyright (c) 2020, Juan (juan.valero@etu.univ-lyon1.fr), All rights reserved
 */

namespace Annotations\Exceptions;

class NotAnnotatedClassException extends AnnotationException
{
    /**
     * NotAnnotatedClassException constructor.
     * @param string $class
     */
    public function __construct(string $class)
    {
        parent::__construct(sprintf('%s must be annotated with @Annotation in order to be treated', $class));
    }
}