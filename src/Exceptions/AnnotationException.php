<?php
/*
 * Copyright (c) 2020, Juan (juan.valero@etu.univ-lyon1.fr), All rights reserved
 */

namespace Annotations\Exceptions;

use Exception;

/**
 * Class AnnotationException
 * Base of annotations exceptions
 *
 * @package Annotation\Exceptions
 */
class AnnotationException extends Exception
{

}
