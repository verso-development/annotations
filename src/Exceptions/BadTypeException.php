<?php
/*
 * Copyright (c) 2020, Juan (juan.valero@etu.univ-lyon1.fr), All rights reserved
 */

namespace Annotations\Exceptions;

use Annotations\Parser\Annotation\Argument;

/**
 * Class BadTypeException
 * Throw when an {@link Argument} value hasn't the expected type
 *
 * @package Annotation\Exceptions
 */
class BadTypeException extends AnnotationException
{
    /**
     * BadTypeException constructor.
     * @param string $expectedType
     * @param string $givenType
     * @param string $property
     */
    public function __construct(string $expectedType, string $givenType, string $property)
    {
        parent::__construct(
            sprintf(
                '%s was expected but %s was given for property %s',
                $expectedType,
                $givenType,
                $property
            )
        );
    }
}
