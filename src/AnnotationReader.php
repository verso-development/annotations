<?php
/*
 * Copyright (c) 2020, Juan (juan.valero@etu.univ-lyon1.fr), All rights reserved
 */

namespace Annotations;

use Annotations\Annotations\Annotation;
use Annotations\Annotations\RuledAnnotation;
use Annotations\Exceptions\AnnotationException;
use Annotations\Exceptions\BadTypeException;
use Annotations\Exceptions\ClassNotFoundException;
use Annotations\Exceptions\ConstructorNotAllowedException;
use Annotations\Exceptions\MissingArgumentsException;
use Annotations\Exceptions\NonExistentPropertyException;
use Annotations\Exceptions\NotAnnotatedClassException;
use Annotations\Exceptions\NotValidatedRuleException;
use ReflectionClass;
use ReflectionException;
use ReflectionMethod;
use ReflectionProperty;
use TypeError;

class AnnotationReader
{
    /**
     * @param ReflectionClass $class
     * @return array
     * @throws ReflectionException|AnnotationException
     */
    public function getClassAnnotations(ReflectionClass $class): array
    {
        return $this->getAnnotations($class, $class);
    }

    /**
     * @param ReflectionClass $class class in which the the entity is located
     * @param ReflectionClass|ReflectionMethod|ReflectionProperty $entity entity in which to read the comment doc
     * @return array
     * @throws ReflectionException|AnnotationException
     */
    private function getAnnotations(ReflectionClass $class, $entity): array
    {
        $annotationsImports = $this->parseAnnotationsImports($class->getFileName(), $class->getStartLine());

        $annotationsParser = new AnnotationProvider($entity->getDocComment());
        $parsedAnnotations = $annotationsParser->parseAnnotations();

        $annotations = [];

        foreach ($parsedAnnotations as $name => $args) {
            if (!class_exists($name) &&
                !class_exists($class->getNamespaceName() . '\\' . $name) &&
                !key_exists($name, $annotationsImports)) {
                throw new ClassNotFoundException();
            }
            $annotationClassName = $annotationsImports[$name]
                ?? (class_exists($name)
                    ? $name
                    : $class->getNamespaceName() . '\\' . $name);
            $annotationClass = new ReflectionClass($annotationClassName);

            if ($annotationClassName !== Annotation::class &&
                !array_key_exists(Annotation::class, $this->getClassAnnotations($annotationClass))) {
                throw new NotAnnotatedClassException($annotationClassName);
            }

            $annotationConstructor = $annotationClass->getConstructor();

            if (!is_null($annotationConstructor) && !empty($annotationConstructor->getParameters())) {
                throw new ConstructorNotAllowedException();
            }

            $instance = $annotationClass->newInstance();

            $reflectionProperties = $annotationClass->getProperties();

            $properties = array_combine(
                array_map(
                    function ($property) {
                        return $property->getName();
                    },
                    $reflectionProperties
                ),
                array_map(
                    function ($property) use ($instance) {
                        if ($property->isInitialized($instance)) {
                            return false;
                        } else {
                            return true;
                        }
                    },
                    $reflectionProperties
                )
            );

            $diff = array_diff_key(
                array_filter(
                    $properties,
                    function ($required) {
                        return $required;
                    }
                ),
                $args
            );

            if (count($diff) != 0) {
                throw new MissingArgumentsException(...array_keys($diff));
            }

            // All required properties are present
            foreach ($args as $argName => $argValue) {
                if (!key_exists($argName, $properties)) {
                    throw new NonExistentPropertyException($annotationClassName, $argName);
                }

                try {
                    $instance->$argName = $argValue;
                } catch (TypeError $e) {
                    throw new BadTypeException(
                        gettype($instance->$argName),
                        gettype($argValue),
                        $argName
                    );
                }
            }

            foreach ($this->getClassAnnotations($annotationClass) as $annotation) {
                if ($annotation instanceof RuledAnnotation) {
                    $dependency = $annotation->getDependency($entity);

                    if (!$dependency->valid()) {
                        throw new NotValidatedRuleException(
                            $dependency->getErrorMessage()
                        );
                    }

                    $rule = $annotation->getRule($entity, $instance);

                    if (!$rule->valid()) {
                        throw new NotValidatedRuleException(
                            $rule->getErrorMessage()
                        );
                    }
                }
            }

            foreach ($annotationClass->getProperties() as $property) {
                foreach ($this->getPropertyAnnotations($property) as $annotation) {
                    if ($annotation instanceof RuledAnnotation) {
                        $dependency = $annotation->getDependency($property);

                        if (!$dependency->valid()) {
                            throw new NotValidatedRuleException(
                                $dependency->getErrorMessage()
                            );
                        }

                        $rule = $annotation->getRule($property, $instance);

                        if (!$rule->valid()) {
                            throw new NotValidatedRuleException(
                                $rule->getErrorMessage()
                            );
                        }
                    }
                }
            }

            $annotations[$annotationClassName] = $instance;
        }

        return $annotations;
    }

    /**
     * @param string $filename
     * @param int $startingLine
     * @return array
     * @throws ReflectionException
     */
    private function parseAnnotationsImports(string $filename, int $startingLine): array
    {
        $importProvider = new ImportProvider($filename, $startingLine);
        return $importProvider->parseImports();
    }

    /**
     * @param ReflectionProperty $property
     * @return array
     * @throws ReflectionException|AnnotationException
     */
    public function getPropertyAnnotations(ReflectionProperty $property): array
    {
        return $this->getAnnotations($property->getDeclaringClass(), $property);
    }

    /**
     * @param ReflectionMethod $method
     * @return array
     * @throws ReflectionException|AnnotationException
     */
    public function getMethodAnnotations(ReflectionMethod $method): array
    {
        return $this->getAnnotations($method->getDeclaringClass(), $method);
    }
}
