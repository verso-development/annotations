<?php
/*
 * Copyright (c) 2020, Juan (juan.valero@etu.univ-lyon1.fr), All rights reserved
 */

namespace Annotations\Parser;

use Annotations\Exceptions\Parsing\ParsingException;
use Annotations\Exceptions\Parsing\UnexpectedTokenException;
use Annotations\Exceptions\Parsing\UnknownTokenException;
use Annotations\Lexer\AbstractLexer;
use Annotations\Lexer\AnnotationLexer;
use Annotations\Token\Token;

/**
 * Class AbstractParser
 * Base parsing class
 * Use {@link AbstractParser::match()} to match any token in a given array
 *
 * @package Annotation\Parser
 */
abstract class AbstractParser
{
    /**
     * Current {@link AbstractLexer} to parse on
     *
     * @var AbstractLexer
     */
    protected AbstractLexer $input;

    /**
     * Current {@link Token} to analyze
     *
     * @var Token
     */
    protected Token $lookahead;

    protected string $current = '';

    /**
     * Parser constructor.
     *
     * @param AbstractLexer $input current lexer to parse on
     * @throws UnknownTokenException
     */
    public function __construct(AbstractLexer $input)
    {
        $this->input = $input;

        $this->consume();
    }

    /**
     * Consume the current lookahead
     *
     * @param bool $skipWhitespaces {@code true} if whitespaces must be skipped, {@code false} otherwise
     * @param bool $updateLookahead
     * @return string the consumed lookahead
     * @throws UnknownTokenException
     */
    protected function consume(bool $skipWhitespaces = true, bool $updateLookahead = true): string
    {
        $lookahead = isset($this->lookahead) ? $this->lookahead->getValue() : ''; // Get current lookahead
        $this->current .= $lookahead;

        if ($skipWhitespaces) {
            $newInput = clone $this->input;
            $token = $newInput->nextToken(false);

            while ($token->getType()->getId() === AnnotationLexer::T_WHITESPACE) {
                $this->current .= $token->getValue();
                $token = $newInput->nextToken(false);
            }
        }

        if ($updateLookahead) {
            $this->lookahead = $this->input->nextToken($skipWhitespaces);
        }

        return $lookahead;
    }

    /**
     * Match any token in the given array and consume it
     *
     * @param int ...$tokens tokens array
     * @return string the consumed lookahead
     * @throws ParsingException
     */
    protected function matchAndConsume(int...$tokens): string
    {
        $this->matchOrThrowException(...$tokens);

        return $this->consume();
    }

    /**
     * Match any token in the given array or throw a {@link UnexpectedTokenException} if no token matches
     *
     * @param int ...$tokens tokens array
     * @throws ParsingException
     */
    protected function matchOrThrowException(int...$tokens)
    {
        if (!$this->match(...$tokens)) {
            $tokenNames = array_map(
                function ($token) {
                    return $this->input->getTokenName($token);
                },
                $tokens
            );

            $this->consume(true, false);

            throw new UnexpectedTokenException(
                $this->current,
                $this->input->getCursor(),
                $this->lookahead->getType()->getName(),
                $tokenNames
            );
        }
    }

    /**
     * Match any token in the given array
     *
     * @param int ...$tokens tokens array
     * @return bool {@code true} if lookahead matches any token, {@code false} otherwise
     */
    protected function match(int...$tokens): bool
    {
        return in_array($this->lookahead->getType()->getId(), $tokens);
    }

    protected function getCurrentTokenId(): int
    {
        return $this->lookahead->getType()->getId();
    }
}
