<?php
/*
 * Copyright (c) 2020, Juan (juan.valero@etu.univ-lyon1.fr), All rights reserved
 */

namespace Annotations\Parser\Annotation;

use Annotations\Config\AnnotationConfig;
use Annotations\Exceptions\Parsing\ParsingException;
use Annotations\Exceptions\Parsing\UnknownTokenException;
use Annotations\Lexer\AnnotationLexer;
use Annotations\Parser\AbstractParser;

/**
 * Class AnnotationParser
 * It represents the annotation parser
 *
 * @package Annotation\Parser\Annotation
 */
class AnnotationParser extends AbstractParser
{
    /**
     * AnnotationProvider constructor.
     * @param AnnotationLexer $lexer
     * @throws UnknownTokenException
     */
    public function __construct(AnnotationLexer $lexer)
    {
        parent::__construct($lexer);
    }

    /**
     * @return Annotation[]
     * @throws ParsingException
     */
    public function parseAnnotations(): array
    {
        $annotations = [];

        $this->matchAndConsume(AnnotationLexer::T_SLASH);
        $this->matchAndConsume(AnnotationLexer::T_ASTERISK);
        $this->matchAndConsume(AnnotationLexer::T_ASTERISK);

        while (!$this->match(AnnotationLexer::T_END)) {
            if ($this->match(AnnotationLexer::T_BEGIN_ANNOTATION)) {
                $this->current = ''; // New current entity
                $this->consume(false);

                $annotations[] = $this->parseAnnotation();
            } else {
                $this->escape();
            }
        }

        return $annotations;
    }

    /**
     * @return Annotation
     * @throws ParsingException
     */
    private function parseAnnotation(): Annotation
    {
        $name = $this->parseIdentifier();

        $args = [];

        if ($this->match(AnnotationLexer::T_OPENING_PARENTHESIS)) {
            $this->consume();

            if (!$this->match(AnnotationLexer::T_CLOSING_PARENTHESIS)) {
                $args = $this->parseArgs();
            }

            $this->matchAndConsume(AnnotationLexer::T_CLOSING_PARENTHESIS);
        }

        return new Annotation($name, $args);
    }

    /**
     * @return string
     * @throws ParsingException
     */
    private function parseIdentifier(): string
    {
        $identifier = $this->matchAndConsume(AnnotationLexer::T_LETTER);

        while ($this->match(AnnotationLexer::T_LETTER, AnnotationLexer::T_NUMBER)) {
            $identifier .= $this->consume(false);
        }

        return $identifier;
    }

    /**
     * @return Argument[]
     * @throws ParsingException
     */
    public function parseArgs(): array
    {
        if ($this->match(AnnotationLexer::T_LETTER)) {
            $args[] = $this->parseArg();

            while ($this->match(AnnotationLexer::T_ARGS_SEPARATOR)) {
                $this->consume();
                $args[] = $this->parseArg();
            }
        } else {
            // Simplified syntax allowed if the annotation has only one required arg named 'value'
            $value = $this->parseValue();
            $args[] = new Argument(AnnotationConfig::SINGLE_PROPERTY_DEFAULT_NAME, $value);

            $this->match(AnnotationLexer::T_CLOSING_PARENTHESIS);
        }

        return $args;
    }

    /**
     * @return Argument
     * @throws ParsingException
     */
    private function parseArg(): Argument
    {
        $identifier = $this->parseIdentifier();
        $this->matchAndConsume(AnnotationLexer::T_VALUE_SEPARATOR);
        $value = $this->parseValue();

        return new Argument($identifier, $value);
    }

    /**
     * @return string
     * @throws ParsingException
     */
    private function parseValue()
    {
        $ALLOWED_CHARACTERS = [
            AnnotationLexer::T_QUOTE,
            AnnotationLexer::T_OPENING_BRACKET,
            AnnotationLexer::T_OPENING_BRACE,
            AnnotationLexer::T_NUMBER
        ];

        $this->matchOrThrowException(...$ALLOWED_CHARACTERS);

        $value = [];

        switch ($this->getCurrentTokenId()) {
            case AnnotationLexer::T_QUOTE:
                $value[] = $this->parseString();

                break;

            case AnnotationLexer::T_OPENING_BRACE:
                $value[] = $this->consume();
                $args = [];

                if ($this->match(AnnotationLexer::T_QUOTE)) {
                    $args[] = $this->parseBraceArg();

                    while ($this->match(AnnotationLexer::T_ARGS_SEPARATOR)) {
                        $this->consume();
                        $args[] = $this->parseBraceArg();
                    }
                }

                $value[] = implode(",", $args);
                $value[] = $this->matchAndConsume(AnnotationLexer::T_CLOSING_BRACE);

                break;

            case AnnotationLexer::T_OPENING_BRACKET:
                $value[] = $this->consume();
                $args = [];

                if ($this->match(...$ALLOWED_CHARACTERS)) {
                    $args[] = $this->parseValue();

                    while ($this->match(AnnotationLexer::T_ARGS_SEPARATOR)) {
                        $this->consume();
                        $args[] = $this->parseValue();
                    }
                }

                $value[] = implode(",", $args);
                $value[] = $this->matchAndConsume(AnnotationLexer::T_CLOSING_BRACKET);

                break;

            case AnnotationLexer::T_NUMBER:
                while ($this->match(AnnotationLexer::T_NUMBER)) {
                    $value[] = $this->consume();
                }

                break;
        }

        return implode($value);
    }

    /**
     * @throws ParsingException
     */
    private function parseBraceArg()
    {
        $identifier = $this->parseString();
        $this->matchAndConsume(AnnotationLexer::T_BRACE_ARGS_SEPARATOR);
        $value = $this->parseValue();

        return $identifier . ':' . $value;
    }

    /**
     * @return string
     * @throws UnknownTokenException
     */
    private function parseString()
    {
        $value = '';

        if ($this->match(AnnotationLexer::T_QUOTE)) {
            $value .= $this->consume();

            do {
                $value .= $this->escape(false);
            } while (!$this->match(AnnotationLexer::T_QUOTE) && !$this->match(AnnotationLexer::T_END));

            $value .= $this->consume();
        }

        return $value;
    }

    /**
     * @param bool $skipWhitespaces
     * @return string
     * @throws UnknownTokenException
     */
    private function escape(bool $skipWhitespaces = true): string
    {
        $value = '';

        if ($this->match(AnnotationLexer::T_BACK_SLASH)) {
            $value .= $this->consume($skipWhitespaces);

            if (!$this->match(AnnotationLexer::T_END)) {
                $value .= $this->consume($skipWhitespaces);
            }
        } else {
            $value .= $this->consume($skipWhitespaces);
        }

        return $value;
    }
}
