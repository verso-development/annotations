<?php
/*
 * Copyright (c) 2020, Juan (juan.valero@etu.univ-lyon1.fr), All rights reserved
 */

namespace Annotations\Parser\Annotation;

/**
 * Class Argument
 * An arguments is part of an {@link Annotation} and is represented as follows :
 * <code>
 * \@Annotation(identifier1=value1[, identifier2=value2 ...])
 * </code>
 * where identifier1 is the identifier of the first argument, value1 the value of the first argument, ...
 *
 * @package Annotation\Parser\Annotation
 */
class Argument
{
    /**
     * Argument identifier
     *
     * @var string
     */
    private string $identifier;

    /**
     * Argument value
     *
     * @var string
     */
    private string $value;

    /**
     * Argument constructor.
     *
     * @param string $identifier identifier of the argument
     * @param string $value value of the argument
     */
    public function __construct(string $identifier, string $value)
    {
        $this->identifier = $identifier;
        $this->value = $value;
    }

    /**
     * Retrieves the argument identifier
     *
     * @return string identifier of the argument
     */
    public function getIdentifier(): string
    {
        return $this->identifier;
    }

    /**
     * Retrieves the argument value
     *
     * @return string value of the argument
     */
    public function getValue(): string
    {
        return $this->value;
    }
}
