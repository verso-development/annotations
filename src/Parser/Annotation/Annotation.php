<?php
/*
 * Copyright (c) 2020, Juan (juan.valero@etu.univ-lyon1.fr), All rights reserved
 */

namespace Annotations\Parser\Annotation;

/**
 * Class Annotation
 * An annotation is represented as follows :
 * <code>
 * \@AnnotationName(args...)
 * </code>
 * where AnnotationName is the annotation name and args, an array of {@link Argument}s
 * @package Annotation\Parser\Annotation
 */
class Annotation
{
    /**
     * The annotation name
     * @var string
     */
    private string $name;

    /**
     * The annotation {@link Argument}s list
     * @var Argument[]
     */
    private array $args;

    /**
     * Annotation constructor.
     * @param string $name name of the annotation
     * @param Argument[] $args arguments list of the annotation
     */
    public function __construct(string $name, array $args)
    {
        $this->name = $name;
        $this->args = $args;
    }

    /**
     * Retrieves the annotation name
     * @return string name of the annotation
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * Retrieves the annotation {@link Argument}s list
     * @return Argument[]
     */
    public function getArgs(): array
    {
        return $this->args;
    }
}
