<?php
/*
 * Copyright (c) 2020, Juan (juan.valero@etu.univ-lyon1.fr), All rights reserved
 */

namespace Annotations\Lexer;

use Annotations\Exceptions\Parsing\UnknownTokenException;
use Annotations\Token\Token;
use Annotations\Token\TokenType;
use IntlChar;
use InvalidArgumentException;

/**
 * Class AbstractLexer
 * @package Annotation\Lexer
 */
abstract class AbstractLexer
{
    public const T_END = -1;
    public const T_UNKNOWN = 0;

    public const T_CHARACTER = 999;

    /**
     * @var TokenType[]
     */
    protected array $TOKENS;

    private string $input;

    private int $index = 0;
    private string $lookahead;

    /**
     * AbstractLexer constructor.
     * @param string $input
     */
    public function __construct(string $input)
    {
        if (strlen($input) === 0) {
            throw new InvalidArgumentException('Input string cannot be empty');
        }

        $this->input = $input;

        $this->init();

        $this->consume();
    }

    protected function init(array $tokens = []): void
    {
        $this->TOKENS = $tokens + [
                self::T_END =>
                    new TokenType(self::T_END, 'T_END', '/END/'),
                self::T_UNKNOWN =>
                    new TokenType(self::T_UNKNOWN, 'T_UNKNOWN', '/UNKNOWN/'),
                self::T_CHARACTER =>
                    new TokenType(self::T_CHARACTER, 'T_CHARACTER', '/./')
            ];
    }

    private function consume(): void
    {
        if ($this->index >= strlen($this->input)) {
            $this->lookahead = self::T_END;
        } else {
            $this->lookahead = substr($this->input, $this->index, 1);
        }

        $this->index++;
    }

    /**
     * @param bool $skipWhitespaces
     * @return Token
     * @throws UnknownTokenException
     */
    public function nextToken(bool $skipWhitespaces = true): Token
    {
        if ($skipWhitespaces) {
            // Ignore white spaces
            while (IntlChar::isblank($this->lookahead) || $this->lookahead === PHP_EOL) {
                $this->consume();
            }
        }

        if ($this->lookahead == self::T_END) {
            $token = new Token($this->TOKENS[-1], '/END/');
        } else {
            $token = new Token($this->TOKENS[0], $this->lookahead);

            foreach ($this->TOKENS as $TOKEN) {
                if ($this->match($TOKEN, $this->lookahead)) {
                    $token = new Token($TOKEN, $this->lookahead);

                    break; // Avoid from matching other patterns
                }
            }
        }

        if ($token->getType()->getId() == self::T_UNKNOWN) {
            throw new UnknownTokenException($this->input, $this->index, $this->lookahead);
        }

        $this->consume();

        return $token;
    }

    private function match(TokenType $token, string $lookahead): bool
    {
        return preg_match($token->getPattern(), $lookahead);
    }

    public function getCursor(): int
    {
        return $this->index;
    }

    public function getTokenName(int $token): string
    {
        return $this->TOKENS[$token]->getName();
    }
}
