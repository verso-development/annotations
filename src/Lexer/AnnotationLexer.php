<?php
/*
 * Copyright (c) 2020, Juan (juan.valero@etu.univ-lyon1.fr), All rights reserved
 */

namespace Annotations\Lexer;

use Annotations\Token\TokenType;

class AnnotationLexer extends AbstractLexer
{
    public const T_WHITESPACE = 1;

    public const T_BACK_SLASH = 10;

    public const T_ASTERISK = 11;
    public const T_SLASH = 12;

    public const T_BEGIN_ANNOTATION = 2;

    public const T_OPENING_PARENTHESIS = 21;
    public const T_CLOSING_PARENTHESIS = 22;

    public const T_NUMBER = 3;
    public const T_LETTER = 4;

    public const T_VALUE_SEPARATOR = 5;
    public const T_ARGS_SEPARATOR = 6;

    public const T_QUOTE = 7;

    public const T_OPENING_BRACKET = 81;
    public const T_CLOSING_BRACKET = 82;

    public const T_BRACE_ARGS_SEPARATOR = 90;
    public const T_OPENING_BRACE = 91;
    public const T_CLOSING_BRACE = 92;

    /**
     * AnnotationLexer constructor.
     * @param string $input
     */
    public function __construct(string $input)
    {
        parent::__construct($input);
    }

    protected function init(array $tokens = []): void
    {
        parent::init(
            [
                self::T_WHITESPACE =>
                    new TokenType(self::T_WHITESPACE, 'T_WHITESPACE', '/\s/'),
                self::T_BACK_SLASH =>
                    new TokenType(self::T_BACK_SLASH, 'T_BACK_SLASH', '/\\\\/'),
                self::T_ASTERISK =>
                    new TokenType(self::T_ASTERISK, 'T_ASTERISK', '/\*/'),
                self::T_SLASH =>
                    new TokenType(self::T_SLASH, 'T_SLASH', '/\//'),
                self::T_BEGIN_ANNOTATION =>
                    new TokenType(self::T_BEGIN_ANNOTATION, 'T_BEGIN_ANNOTATION', '/@/'),
                self::T_OPENING_PARENTHESIS =>
                    new TokenType(self::T_OPENING_PARENTHESIS, 'T_OPENING_PARENTHESIS', '/\(/'),
                self::T_CLOSING_PARENTHESIS =>
                    new TokenType(self::T_CLOSING_PARENTHESIS, 'T_CLOSING_PARENTHESIS', '/\)/'),
                self::T_NUMBER =>
                    new TokenType(self::T_NUMBER, 'T_NUMBER', '/\d/'),
                self::T_LETTER =>
                    new TokenType(self::T_LETTER, 'T_LETTER', '/[A-Za-z]/'),
                self::T_VALUE_SEPARATOR =>
                    new TokenType(self::T_VALUE_SEPARATOR, 'T_VALUE_SEPARATOR', '/=/'),
                self::T_ARGS_SEPARATOR =>
                    new TokenType(self::T_ARGS_SEPARATOR, 'T_ARGS_SEPARATOR', '/,/'),
                self::T_QUOTE =>
                    new TokenType(self::T_QUOTE, 'T_QUOTE', '/"/'),
                self::T_OPENING_BRACKET =>
                    new TokenType(self::T_OPENING_BRACKET, 'T_OPENING_BRACKET', '/\[/'),
                self::T_CLOSING_BRACKET =>
                    new TokenType(self::T_CLOSING_BRACKET, 'T_CLOSING_BRACKET', '/]/'),
                self::T_BRACE_ARGS_SEPARATOR =>
                    new TokenType(self::T_BRACE_ARGS_SEPARATOR, 'T_BRACE_ARGS_SEPARATOR', '/:/'),
                self::T_OPENING_BRACE =>
                    new TokenType(self::T_OPENING_BRACE, 'T_OPENING_BRACE', '/{/'),
                self::T_CLOSING_BRACE =>
                    new TokenType(self::T_CLOSING_BRACE, 'T_CLOSING_BRACE', '/}/')
            ]
        );
    }
}
