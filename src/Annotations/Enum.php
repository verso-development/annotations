<?php
/*
 * Copyright (c) 2020, Juan (juan.valero@etu.univ-lyon1.fr), All rights reserved
 */

namespace Annotations\Annotations;

use Annotations\Annotations\Rules\EnumRule;
use Annotations\Annotations\Rules\Rule;
use Annotations\Annotations\Rules\TargetRule;

/**
 * Class Enum
 *
 * If a property is annotated with {@link Enum}, his value can only be one of the values
 * given in the {@link Enum::$value} array
 *
 * Example :
 * <code>
 * class MyAnnotation {
 * \@Enum(["ONE", "TWO"])
 * public string $value;
 * }
 * </code>
 * <code>
 * \@MyAnnotation("ONE") -> OK
 * \@MyAnnotation("THREE") -> NO
 * </code>
 *
 * Note that this annotation can only be placed on properties
 *
 * @package Annotations\Annotations;
 *
 * @Annotation
 */
class Enum implements RuledAnnotation
{
    /**
     * Array of allowed values
     *
     * @var array
     */
    public array $value;

    public function getRule($entity, ?object $annotation): Rule
    {
        return new EnumRule($this->value, $entity, $annotation);
    }

    public function getDependency($entity): Rule
    {
        return new TargetRule(['PROPERTY'], $entity, $this);
    }
}
