<?php
/*
 * Copyright (c) 2020, Juan (juan.valero@etu.univ-lyon1.fr), All rights reserved
 */

namespace Annotations\Annotations;

use Annotations\Annotations\Rules\RegexRule;
use Annotations\Annotations\Rules\Rule;
use Annotations\Annotations\Rules\TargetRule;

/**
 * Class Regex
 *
 * If a property is annotated with {@link Regex}, his value must be match the regex
 * given in the {@link Enum::$value}
 *
 * Example :
 * <code>
 * class MyAnnotation {
 * \@Regex("/[a-z]{2,}/") - At least 2 lowercase letters
 * public string $value;
 * }
 * </code>
 * <code>
 * \@MyAnnotation("c") -> NO
 * \@MyAnnotation("cat") -> YES
 * </code>
 *
 * Note that this annotation can only be placed on properties
 *
 * @package Annotations\Annotations;
 *
 * @Annotation
 */
class Regex implements RuledAnnotation
{
    /**
     * Regex to match
     *
     * @var string
     */
    public string $value;

    public function getRule($entity, ?object $annotation): Rule
    {
        return new RegexRule($this->value, $entity, $annotation);
    }

    public function getDependency($entity): Rule
    {
        return new TargetRule(['PROPERTY'], $entity, null);
    }
}
