<?php
/*
 * Copyright (c) 2020, Juan (juan.valero@etu.univ-lyon1.fr), All rights reserved
 */

namespace Annotations\Annotations;

use Annotations\Annotations\Rules\Rule;

interface RuledAnnotation
{
    public function getRule($entity, ?object $annotation): Rule;

    public function getDependency($entity): Rule;
}
