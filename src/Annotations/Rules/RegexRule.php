<?php
/*
 * Copyright (c) 2020, Juan (juan.valero@etu.univ-lyon1.fr), All rights reserved
 */

namespace Annotations\Annotations\Rules;

class RegexRule extends Rule
{
    /**
     * @var string
     */
    private string $value;

    /**
     * RegexRule constructor.
     * @param string $value
     * @param $entity
     * @param $annotation
     */
    public function __construct(string $value, $entity, $annotation)
    {
        $this->value = $value;
        parent::__construct($entity, $annotation);
    }

    /**
     * @inheritDoc
     */
    public function valid(): bool
    {
        $value = $this->entity->getName();

        return preg_match($this->value, $this->annotation->$value);
    }

    /**
     * @inheritDoc
     */
    public function getErrorMessage(): string
    {
        return sprintf('@Regex error : No match for %s', $this->value);
    }
}
