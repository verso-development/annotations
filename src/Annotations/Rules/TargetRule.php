<?php
/*
 * Copyright (c) 2020, Juan (juan.valero@etu.univ-lyon1.fr), All rights reserved
 */

namespace Annotations\Annotations\Rules;

use ReflectionClass;
use ReflectionMethod;
use ReflectionProperty;

class TargetRule extends Rule
{
    /**
     * Array of allowed locations
     *
     * @var array
     */
    public array $values;

    /**
     * EnumRule constructor.
     * @param array $values
     * @param $entity
     * @param $annotation
     */
    public function __construct(array $values, $entity, $annotation)
    {
        $this->values = $values;
        parent::__construct($entity, $annotation);
    }

    /**
     * @inheritDoc
     */
    public function valid(): bool
    {
        return in_array($this->getType($this->entity), $this->values);
    }

    private function getType($entity): ?string
    {
        if ($entity instanceof ReflectionClass) {
            return 'CLASS';
        } elseif ($entity instanceof ReflectionMethod) {
            return 'METHOD';
        } elseif ($entity instanceof ReflectionProperty) {
            return 'PROPERTY';
        } else {
            return null;
        }
    }

    /**
     * @inheritDoc
     */
    public function getErrorMessage(): string
    {
        $targets = implode(' or ', $this->values);

        return sprintf(
            '@Target error : %s is expected to be placed on %s, found %s',
            get_class($this->annotation),
            $targets,
            $this->getType($this->entity)
        );
    }
}
