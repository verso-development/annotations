<?php
/*
 * Copyright (c) 2020, Juan (juan.valero@etu.univ-lyon1.fr), All rights reserved
 */

namespace Annotations\Annotations\Rules;

use ReflectionClass;
use ReflectionMethod;
use ReflectionProperty;

abstract class Rule
{
    /**
     * @var ReflectionClass|ReflectionMethod|ReflectionProperty
     */
    protected $entity;

    /**
     * @var object|null
     */
    protected ?object $annotation;

    /**
     * Rule constructor.
     * @param ReflectionClass|ReflectionMethod|ReflectionProperty $entity
     * @param object|null $annotation
     */
    public function __construct($entity, ?object $annotation)
    {
        $this->entity = $entity;
        $this->annotation = $annotation;
    }

    /**
     * @return bool
     */
    abstract public function valid(): bool;

    /**
     * @return string
     */
    abstract public function getErrorMessage(): string;
}
