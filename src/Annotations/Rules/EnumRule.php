<?php
/*
 * Copyright (c) 2020, Juan (juan.valero@etu.univ-lyon1.fr), All rights reserved
 */

namespace Annotations\Annotations\Rules;

class EnumRule extends Rule
{
    private array $values;

    /**
     * EnumRule constructor.
     * @param array $values
     * @param $entity
     * @param $annotation
     */
    public function __construct(array $values, $entity, $annotation)
    {
        $this->values = $values;
        parent::__construct($entity, $annotation);
    }

    public function valid(): bool
    {
        $entityName = $this->entity->getName();
        $property = $this->annotation->$entityName;

        $values = is_array($property)
            ? $property
            : [$property];

        foreach ($values as $value) {
            if (!in_array($value, $this->values)) {
                return false;
            }
        }

        return true;
    }

    public function getErrorMessage(): string
    {
        $value = array_map(
            function ($v) {
                return sprintf('"%s"', $v);
            },
            $this->values
        );

        $enums = implode(' or ', $value);

        $entityName = $this->entity->getName();
        $property = $this->annotation->$entityName;

        if (is_array($property)) {
            $property = '[' . implode(', ', $property) . ']';
        }

        return sprintf('@Enum error : Expected %s, found %s', $enums, $property);
    }
}
