<?php
/*
 * Copyright (c) 2020, Juan (juan.valero@etu.univ-lyon1.fr), All rights reserved
 */

namespace Annotations\Annotations;

use Annotations\Annotations\Rules\EnumRule;
use Annotations\Annotations\Rules\Rule;
use Annotations\Annotations\Rules\TargetRule;
use ReflectionProperty;

/**
 * Class Target
 *
 * If an annotation class is annotated with {@link Target}, it can only be placed on one of the locations
 * given in the {@link Enum::$value} array
 *
 * Allowed values are :
 * - CLASS : Annotation must be placed above a class declaration
 * - METHOD : Annotation must be placed above a method declaration
 * - PROPERTY : Annotation must be placed above a property declaration
 *
 * Example :
 * <code>
 * \@Target(["CLASS"])
 * class MyAnnotation {
 * }
 * </code>
 * <code>
 * \@MyAnnotation
 * class ExampleA {
 * } -> OK
 *
 * class ExampleB {
 * \@MyAnnotation
 * private int $value;
 * } -> NO
 * </code>
 *
 * @package Annotations\Annotations
 *
 * @Annotation
 */
class Target implements RuledAnnotation
{
    /**
     * Array of allowed locations
     *
     * @var array
     */
    public array $value;

    public function getRule($entity, ?object $annotation): Rule
    {
        return new TargetRule($this->value, $entity, $annotation);
    }

    public function getDependency($entity): Rule
    {
        return new EnumRule(
            ['CLASS', 'METHOD', 'PROPERTY'],
            new ReflectionProperty(self::class, 'value'),
            $this
        );
    }
}
