<?php
/*
 * Copyright (c) 2020, Juan (juan.valero@etu.univ-lyon1.fr), All rights reserved
 */

namespace Annotations;

use Annotations\Exceptions\Parsing\ParsingException;
use Annotations\Lexer\AnnotationLexer;
use Annotations\Parser\Annotation\AnnotationParser;

class AnnotationProvider
{
    /**
     * @var array
     */
    private static array $ignoredAnnotations = [
        'api',
        'author',
        'copyright',
        'deprecated',
        'inheritDoc',
        'internal',
        'link',
        'method',
        'package',
        'param',
        'property',
        'return',
        'see',
        'since',
        'throws',
        'todo',
        'uses',
        'var',
        'version'
    ];

    /**
     * @var string
     */
    private string $docComment;

    /**
     * AnnotationsParser constructor.
     * @param string $docComment
     */
    public function __construct(string $docComment)
    {
        $this->docComment = $docComment;
    }

    /**
     * @param bool $ignoreBuiltInAnnotations
     * @return array
     * @throws ParsingException
     */
    public function parseAnnotations(bool $ignoreBuiltInAnnotations = true): array
    {
        $annotations = [];

        if ($this->docComment) {
            $lexer = new AnnotationLexer($this->docComment);
            $parser = new AnnotationParser($lexer);

            $matchedAnnotations = $parser->parseAnnotations();

            if ($ignoreBuiltInAnnotations) {
                // Only keep annotations that are not ignored
                $matchedAnnotations = array_filter(
                    $matchedAnnotations,
                    function ($matchedAnnotation) {
                        return !(in_array($matchedAnnotation->getName(), self::$ignoredAnnotations));
                    }
                );
            }

            foreach ($matchedAnnotations as $matchedAnnotation) {
                $parsedArgs = [];

                foreach ($matchedAnnotation->getArgs() as $arg) {
                    // Parse each value using json and bind it
                    $parsedArgs[$arg->getIdentifier()] = json_decode($arg->getValue(), true);
                }

                $annotations[$matchedAnnotation->getName()] = $parsedArgs;
            }
        }

        return $annotations;
    }
}
