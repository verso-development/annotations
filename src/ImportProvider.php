<?php
/*
 * Copyright (c) 2020, Juan (juan.valero@etu.univ-lyon1.fr), All rights reserved
 */

namespace Annotations;

use ReflectionClass;
use ReflectionException;
use SplFileObject;

class ImportProvider
{
    /**
     * @var string
     */
    private string $filename;

    /**
     * @var int
     */
    private int $startLine;

    /**
     * PHPParser constructor.
     * @param string $filename
     * @param int $startLine
     */
    public function __construct(string $filename, int $startLine)
    {
        $this->filename = $filename;
        $this->startLine = $startLine;
    }

    /**
     * @return array
     * @throws ReflectionException
     */
    public function parseImports(): array
    {
        $content = $this->parseFile($this->startLine);

        preg_match_all("#use\s+([a-zA-Z\\\\]+)(?:\s+as\s+([a-zA-Z]+))?#i", $content, $matchedImports, PREG_SET_ORDER);

        $imports = [];
        foreach ($matchedImports as $matchedImport) {
            $path = $matchedImport[1];
            $alias = $matchedImport[2] ?? (new ReflectionClass($path))->getShortName();

            $imports[$alias] = $path;
        }

        return $imports;
    }

    /**
     * @param int $nbl
     * @return string
     */
    private function parseFile(int $nbl): string
    {
        $file = new SplFileObject($this->filename, 'r');

        $content = '';
        $lines = 0;
        while (($line = $file->fgets()) && $lines++ < $nbl) {
            $content .= $line;
        }

        return $content;
    }
}
