<?php
/*
 * Copyright (c) 2020, Juan (juan.valero@etu.univ-lyon1.fr), All rights reserved
 */

namespace Annotations\Config;

class AnnotationConfig
{
    public const SINGLE_PROPERTY_DEFAULT_NAME = 'value';
}